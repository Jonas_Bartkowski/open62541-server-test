#include "open62541.h"
#include "s-store.h"

#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

UA_Server* currentServer;
UA_ServerConfig* currentConfig;
UA_HistoryDataGathering* currentGathering;
UA_HistorizingNodeIdSettings* currentSetting;
UA_NodeId* storedNodes;
bool debug = true;

void instantiateSetting()
{
    UA_HistoryDataGathering gathering = *currentGathering;
    UA_HistorizingNodeIdSettings setting;
    /* Use the default sample in-memory database. Reserve space for 3 nodes with an initial room of 100 values each.
    * This will also automaticaly grow if needed. */
    setting.historizingBackend = UA_HistoryDataBackend_Memory(3, 10);
     
    /* We want the server to serve a maximum of 100 values per request.
     * This value depends on the platform you are running the server on - a big
     * server can serve more values in a single request. */
    setting.maxHistoryDataResponseSize = 10;
    // update the database each time the node value is set
    setting.historizingUpdateStrategy = UA_HISTORIZINGUPDATESTRATEGY_VALUESET;
    currentSetting = &setting;
}

void
registerNodeWithGathering(UA_NodeId* outNodeId)
{
    UA_HistorizingNodeIdSettings setting = *currentSetting;
    UA_HistoryDataGathering gathering = *currentGathering;
    // register the node for gathering data in the database
    UA_StatusCode retval = gathering.registerNodeId(currentServer, gathering.context, outNodeId, setting);

    fprintf(stderr, "registerNodeId %s\n", UA_StatusCode_name(retval));
}

UA_NodeId*
addVariableAttribute(UA_VariableAttributes* attrp, const UA_DataType* type, UA_NodeId id, UA_QualifiedName name)
{
    UA_VariableAttributes attr = *attrp;
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_NodeId outNodeId;

    UA_NodeId* outp = (UA_NodeId*) malloc(sizeof(UA_NodeId));
    outp = &outNodeId;
    UA_NodeId_init(outp);

    UA_Server_addVariableNode(currentServer, 
                            id, 
                            parentNodeId,
                            parentReferenceNodeId, 
                            name,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), 
                            attr, 
                            NULL, 
                            outp);

    if (attr.historizing)
    {
        //variable is correctly transmitted
        registerNodeWithGathering(outp);
    }

    if (debug)
    	printf("Added node of type %d successfully!\n", type->typeId.identifierType);

    return outp;
}

UA_VariableAttributes* configureVariableAttributes(UA_VariableAttributes* attrp, const UA_DataType* type, UA_LocalizedText description, bool historization)
{
    //UA_VariableAttributes attr = *attrp;
    attrp->description = description;
    attrp->displayName = description;
    attrp->dataType = type->typeId;
    if (historization)
        attrp->accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE | UA_ACCESSLEVELMASK_HISTORYREAD;
    else
        attrp->accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    attrp->historizing = historization;
    return attrp;
}

UA_NodeId*
addVariable(const UA_DataType* type, UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, void *defaultValue, bool historization)
{
	/* Define the attribute of the myInteger variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&attr.value, defaultValue, type);
    printf("Should configure, attr.historization to :%d\n", historization);
    configureVariableAttributes(&attr, type, description, historization);

    printf("After configuration, attr.historization is :%d\n", attr.historizing);
    return addVariableAttribute(&attr, type, id, name);
}

UA_NodeId*
addArrayVariable(const UA_DataType* type, UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, void* defaultValue, int defaultSize, bool historization)
{
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Variant_setArrayCopy(&attr.value, defaultValue, defaultSize, type);
    printf("Should configure, attr.historization to :%d\n", historization);
    configureVariableAttributes(&attr, type, description, historization);

    printf("After configuration, attr.historization is :%d\n", attr.historizing);
    return addVariableAttribute(&attr, type, id, name);
}

UA_NodeId*
addIntVariable(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, int defaultValue, bool historization) {
    return addVariable(&UA_TYPES[UA_TYPES_INT32], id, name, description, displayName, (void *) &defaultValue, historization);
}

UA_NodeId*
addIntVariable2(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, int defaultValue)
{
    return addIntVariable(id, name, description, displayName, defaultValue, false);
}

UA_NodeId*
addIntVariable3(int id, char* id_str, char* name, int defaultValue, bool historization)
{
    return addIntVariable(UA_NODEID_STRING(id, id_str), UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), defaultValue, historization);
}

UA_NodeId*
addIntVariable4(int id, char* id_str, char* name, int defaultValue)
{
    return addIntVariable(UA_NODEID_STRING(id, id_str), UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), defaultValue, false);
}

UA_NodeId*
addStringVariable(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, char* defaultValue, bool historization) 
{
	UA_String str = UA_String_fromChars(defaultValue);
    return addVariable(&UA_TYPES[UA_TYPES_STRING], id, name, description, displayName, (void *) &str, historization);
}

UA_NodeId*
addStringVariable2(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, char* defaultValue)
{
    return addStringVariable(id, name, description, displayName, defaultValue, false);
}

UA_NodeId*
addStringVariable3(int id, char* id_str, char* name, char* defaultValue, bool historization)
{
    return addStringVariable(UA_NODEID_STRING(id, id_str), UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), defaultValue, historization);
}

UA_NodeId*
addStringVariable4(int id, char* id_str, char* name, char* defaultValue)
{
    return addStringVariable(UA_NODEID_STRING(id, id_str), UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), defaultValue, false);
}

UA_NodeId*
addBooleanVariable(int id, char* id_str, char* name, bool defaultValue, UA_Boolean historization)
{
    return addVariable(&UA_TYPES[UA_TYPES_BOOLEAN], UA_NODEID_STRING(id, id_str), UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), (void *) &defaultValue, historization);
}

UA_NodeId*
addStringVariable5(int id, UA_NodeId nid, char* name, char* defaultValue)
{
    return addStringVariable(nid, UA_QUALIFIEDNAME(id, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), defaultValue, false);
}

UA_NodeId*
addStringArrayVariable2(UA_NodeId nid, char* name, UA_String defaultValue[], int defaultSize, bool historization)
{
    //Values are correctly passed here
    return addArrayVariable(&UA_TYPES[UA_TYPES_STRING], nid, UA_QUALIFIEDNAME(nid.namespaceIndex, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), (void *) defaultValue, defaultSize, historization);
}

UA_NodeId*
addStringArrayVariable(int id, char* id_str, char* name, UA_String defaultValue[], int defaultSize, bool historization)
{
    return addStringArrayVariable2(UA_NODEID_STRING(id, id_str), name, defaultValue, defaultSize, historization);
}

UA_NodeId*
addStringArrayVariable4(UA_NodeId nid, char* name, char* defaultValue[], int defaultSize, bool historization)
{
    //Values are correctly passed here
    UA_String arr[defaultSize];
    for (int i = 0; i < defaultSize; i++)
    {
        arr[i] = UA_String_fromChars(defaultValue[i]);
    }
    return addArrayVariable(&UA_TYPES[UA_TYPES_STRING], nid, UA_QUALIFIEDNAME(nid.namespaceIndex, name), UA_LOCALIZEDTEXT("en-US", name), UA_LOCALIZEDTEXT("en-US", name), (void *) arr, defaultSize, historization);
}

UA_NodeId*
addStringArrayVariable3(int id, char* id_str, char* name, char* defaultValue[], int defaultSize, bool historization)
{
    return addStringArrayVariable4(UA_NODEID_STRING(id, id_str), name, defaultValue, defaultSize, historization);
}

void
changeArrayVariable(UA_NodeId nodeId, const UA_DataType* type, void* value, int size)
{
    UA_Variant myVar;
    UA_Variant_setArrayCopy(&myVar, value, size, type);
    UA_Server_writeValue(currentServer, nodeId, myVar);
}

void
changeVariable(UA_NodeId nodeId, const UA_DataType* type, void *value)
{
    UA_Variant myVar;
    UA_Variant_init(&myVar);
    UA_Variant_setScalar(&myVar, value, type);
    UA_Server_writeValue(currentServer, nodeId, myVar);
}

void
changeInt32Variable(UA_NodeId myIntegerNodeId, int val)
{
    changeVariable(myIntegerNodeId, &UA_TYPES[UA_TYPES_INT32], (void *) &val);
}

void
changeInt32Variable2(int id, char* strid, int val)
{
    changeInt32Variable(UA_NODEID_STRING(id, strid), val);
}

void
changeStringVariable(UA_NodeId myStringNodeId, char* val)
{
    UA_String str = UA_String_fromChars(val);
    changeVariable(myStringNodeId, &UA_TYPES[UA_TYPES_STRING], (void *) &str);
}

void
changeStringVariable2(int id, char* strid, char* val)
{

    changeStringVariable(UA_NODEID_STRING(id, strid), val);
}

void
changeBooleanVariable(int id, char* strid, bool val)
{
	changeVariable(UA_NODEID_STRING(id, strid), &UA_TYPES[UA_TYPES_BOOLEAN], (void *) &val);
}

void
changeStringArrayVariable2(UA_NodeId nid, UA_String* value, int size)
{
    changeArrayVariable(nid, &UA_TYPES[UA_TYPES_STRING], value, size);
}

void
changeStringArrayVariable(int id, char* strid, UA_String* value, int size)
{
    changeStringArrayVariable2(UA_NODEID_STRING(id, strid), value, size);
}

void
changeStringArrayVariable4(UA_NodeId nid, char* value[], int size)
{
    UA_String arr[size];
    for (int i = 0; i < size; i++)
    {
        arr[i] = UA_String_fromChars(value[i]);
    }
    changeStringArrayVariable2(nid, arr, size);
}

void
changeStringArrayVariable3(int id, char* strid, char* value[], int size)
{
    changeStringArrayVariable4(UA_NODEID_STRING(id, strid), value, size);
}

int nsleep(long nsec)
{
    struct timespec ts;
    int res;

    if (nsec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = 0;
    ts.tv_nsec = nsec;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}

UA_Boolean
allowAddNode(UA_Server *server, UA_AccessControl *ac,
             const UA_NodeId *sessionId, void *sessionContext,
             const UA_AddNodesItem *item) {
    printf("Called allowAddNode\n");
    return UA_TRUE;
}

UA_Boolean
allowAddReference(UA_Server *server, UA_AccessControl *ac,
                  const UA_NodeId *sessionId, void *sessionContext,
                  const UA_AddReferencesItem *item) {
    printf("Called allowAddReference\n");
    return UA_TRUE;
}

UA_Boolean
allowDeleteNode(UA_Server *server, UA_AccessControl *ac,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_DeleteNodesItem *item) {
    printf("Called allowDeleteNode\n");
    return UA_FALSE; // Do not allow deletion from client
}

UA_Boolean
allowDeleteReference(UA_Server *server, UA_AccessControl *ac,
                     const UA_NodeId *sessionId, void *sessionContext,
                     const UA_DeleteReferencesItem *item) {
    printf("Called allowDeleteReference\n");
    return UA_TRUE;
}

UA_StatusCode initiateAccessControl(UA_UsernamePasswordLogin logins[2])
{
    UA_ServerConfig* config = currentConfig;
    /* Disable anonymous logins, enable two user/password logins */
    config->accessControl.deleteMembers(&config->accessControl);
    UA_StatusCode retval = UA_AccessControl_default(config, false,
             &config->securityPolicies[config->securityPoliciesSize-1].policyUri, 2, logins);
    if(retval != UA_STATUSCODE_GOOD)
        return retval;

    /* Set accessControl functions for nodeManagement */
    config->accessControl.allowAddNode = allowAddNode;
    config->accessControl.allowAddReference = allowAddReference;
    config->accessControl.allowDeleteNode = allowDeleteNode;
    config->accessControl.allowDeleteReference = allowDeleteReference;
    return retval;
}


void printStoredNode(int i)
{
    printf("Variable with type %d and id %d-%s!\n", 
                                storedNodes[i].identifierType,
                                storedNodes[i].namespaceIndex, 
                                storedNodes[i].identifier.byteString.data);
}