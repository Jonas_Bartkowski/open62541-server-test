#include "open62541.h"
#include "s-store.h"
//#include "plugins/historydata/ua_history_database_default.c"

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>

static volatile UA_Boolean running = true;

static void stopHandler(int sig) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "received ctrl-c");
    running = false;
}

void threadTest()
{
    sleep(1);
    if (currentServer == NULL)
        printf("currentServer == null\n");
    else
        printf("Server set-up correctly...\n");

    changeInt32Variable2(1, "the.answer", 50);

    /* NodeId of the variable holding the current time */
    const UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "the.answer");
    UA_Variant value;
    UA_StatusCode retval = UA_Server_readValue(currentServer, myIntegerNodeId, &value);

    if(retval == UA_STATUSCODE_GOOD)
    {
        if (UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_INT32]))
        {
            UA_Int32 answer = *(UA_Int32 *) value.data;
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "answer is: %d\n", answer);
        }
        else
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "no scalar type\n");
    }
    else
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "status code bad\n");
}


static UA_UsernamePasswordLogin logins[2] = 
{
    {UA_STRING_STATIC("peter"), UA_STRING_STATIC("peter123")},
    {UA_STRING_STATIC("paula"), UA_STRING_STATIC("paula123")}
};

UA_NodeId*
addTestVariable(int defaultValue)
{
    return addIntVariable(UA_NODEID_STRING(1, "the.answer"), UA_QUALIFIEDNAME(1, "the answer"), UA_LOCALIZEDTEXT("en-US","the answer"), UA_LOCALIZEDTEXT("en-US","the answer"), defaultValue, true);
}

void initStringNodes()
{
    printf("Initializing String nodes!\n");
    UA_NodeId* strId = addStringVariable3(0, "the.string", "the-string", "abc", true);
}

void initBooleanNodes()
{
    printf("Initializing Boolean nodes!\n");
    UA_NodeId* strId = addBooleanVariable(0, "the.bool", "the-bool", true, true);
}

UA_String toUA_String(char* str)
{
    UA_String uas;
    UA_String_init(&uas);
    uas.length = strlen(str);
    uas.data = (UA_Byte*) str;
    return uas;
}

void initStringArrayNodes()
{
    printf("Initializing String Array nodes!\n");
    UA_String array[2] = {toUA_String("test String one"), toUA_String("test String two")};
    UA_NodeId* strId = addStringArrayVariable(0, "the.strarr", "the-strarr", array, 2, true);
}

void testString()
{
    printf("Testing String nodes!\n");
    sleep(4);
    int maxChanges = 5;
    int c = 0;

    char* str;
    size_t sz;
    while (c < maxChanges)
    {
        sleep(2);
        sz = snprintf(NULL, 0, "abc%d", c);
        str = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
        snprintf(str, sz+1, "abc%d", c);
        printf("Should be changing string test value to %s\n", str);
        changeStringVariable2(0, "the.string", str);
        c++;
    }
}

void testBools()
{
    printf("Testing Boolean nodes!\n");
    sleep(4);
    int maxChanges = 5;
    int c = 0;
    bool std = true;

    char* str;
    size_t sz;
    while (c < maxChanges)
    {
        sleep(2);
        std = !std;
        printf("Should be changing bool test value to %d\n", std);
        changeBooleanVariable(0, "the.bool", std);
        c++;
    }
}

void testStringArrays()
{
    printf("Testing String Array nodes!\n");
    sleep(6);
    int maxChanges = 5;
    int c = 0;
    bool std = false;
    UA_String changedArr[2] = {toUA_String("Changed String One"), toUA_String("Changed String Two")};
    UA_String newArr[2] = {toUA_String("test String one"), toUA_String("test String two")};

    size_t sz;
    while (c < maxChanges)
    {
        sleep(2);
        printf("Should be changing string array test value to array #%d\n", std);
        changeStringArrayVariable(0, "the.strarr", std ? changedArr : newArr, 2);
        c++;
        std = !std;
    }
}

void randomNumbers(int nodesNum, long millisBetweenChanges, long maxChanges, long millisBetweenBursts, long maxBursts, int debug)
{    
    long mChanges = maxChanges > 0 ? maxChanges : 1;
    millisBetweenChanges = millisBetweenChanges >= 0 ? millisBetweenChanges : 1000;
    millisBetweenBursts = millisBetweenBursts >= 0 ? millisBetweenBursts : 5000;
    
    srand(time(NULL));
    long bursts = 0;
    long changes = 0;
    while (bursts < maxBursts || maxBursts <= 0)
    {
        while (changes < mChanges)
        {   // Initialization, should only be called once.
            for (int i = 0; i < nodesNum; i++)
            {
                UA_NodeId nid = storedNodes[i]; 
                int r = rand();
                if (debug)  
                {
                    printf("node %d-%ld/%ld: Changing type %d %d-%s to %d!\n", 
                                i,
                                changes+1, 
                                mChanges, 
                                nid.identifierType,
                                nid.namespaceIndex, 
                                nid.identifier.byteString.data, 
                                r);
                }
                //UA_NodeId_copy(nid, &toUse);
                changeInt32Variable(storedNodes[i], r);
                //changeInt32Variable2(nid->namespaceIndex, nid->identifier.byteString.data, r);
            }
            msleep(millisBetweenChanges);
            changes++;
        }
        changes = 0;
        msleep(millisBetweenBursts);
        bursts++;
    }
}

struct thread_data
{
   long millisBetweenChanges;
   long maxChanges;
   long millisBetweenBursts;
   long maxBursts;
   int debug;
   int numNodes;
   bool testString;
};

void initIntNodes(int numTestVals, int namespaceIndex)
{
    storedNodes = (UA_NodeId*) malloc(sizeof(UA_NodeId) * numTestVals);
    printf("Allocating %d test values!\n", numTestVals);
    for (int i = 0; i < numTestVals; i++)
    { 
        char *strid; char* strname;
        size_t sz;

        sz = snprintf(NULL, 0, "the.answer%d", i);
        strid = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
        snprintf(strid, sz+1, "the.answer%d", i);

        sz = snprintf(NULL, 0, "the answer%d", i);
        strname = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
        snprintf(strname, sz+1, "the answer%d", i);

        UA_NodeId* nid = addIntVariable3(namespaceIndex, strid, strname, 42, true);
        storedNodes[i] = *nid;
        printStoredNode(i);
    }
}

void* changesThread(void *vargp)
{ 
    sleep(4);
    struct thread_data* td = (struct thread_data*) vargp;
    initIntNodes(td->numNodes, 0);
    if (td->testString)
    {
        initStringNodes();
        initBooleanNodes();
        initStringArrayNodes();
    }
    if (td->debug)
        printf("nodes=%d, mbc=%ld, mxc=%ld, mbb=%ld, mxb=%ld\n", td->numNodes, td->millisBetweenChanges, td->maxChanges, td->millisBetweenBursts, td->maxBursts);
    
    //threadTest();
    //printStoredNode(0);
    if (td->testString)
    {
        testString();
        testBools();
        testStringArrays();
    }
    randomNumbers(td->numNodes, td->millisBetweenChanges, td->maxChanges, td->millisBetweenBursts, td->maxBursts, td->debug);
    return NULL; 
} 

int main(int argc, char *argv[]) {
    long millisBetweenChanges = 1000;
    long maxChanges = 1;
    long millisBetweenBursts = 0;
    long maxBursts = 0;
    bool testString = false;
    //int debug = 1;
    int numTestVals = 1;
    //printf("argc = %d\n", argc);

    int opt;
    char *ptr;
    while ((opt = getopt(argc, argv, ":dsv:c:b:")) != -1)
    {
        switch (opt)
        {
            case 'd': 
                debug = 0;
                break;
            case 'v':  
                numTestVals = atoi(optarg);
                break;
            case 'c':  
                millisBetweenChanges = strtol(optarg, &ptr, 10);
                break;
            case 'b':  
                millisBetweenBursts = strtol(optarg, &ptr, 10);
                break; 
            case 's':
                testString = true;
                break;
            case ':':  
                printf("option needs a value\n");  
                break;  
            case '?':  
                printf("unknown option: %c\n", optopt); 
                break;     
        }
    }

    if (debug)
        printf("numTestVals: %d\n", numTestVals);  

    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);
 
    currentServer = UA_Server_new();
    UA_ServerConfig* config = UA_Server_getConfig(currentServer);
    UA_ServerConfig_setDefault(config);
    currentConfig = config;

    UA_StatusCode retval = initiateAccessControl(logins);
    if(retval != UA_STATUSCODE_GOOD)
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "AccessControl initiation failed!\n");


    /* We need a gathering for the plugin to constuct.
     * The UA_HistoryDataGathering is responsible to collect data and store it to the database.
     * We will use this gathering for one node, only. initialNodeIdStoreSize = 1
     * The store will grow if you register more than one node, but this is expensive. */
    UA_HistoryDataGathering gathering = UA_HistoryDataGathering_Default(2);
    currentGathering = &gathering;
    // configure the server with an instance of the UA_HistoryDatabase plugin
    currentConfig->historyDatabase = UA_HistoryDatabase_default(gathering);
    //instantiateSetting();

    UA_HistorizingNodeIdSettings setting;
    /* Use the default sample in-memory database. Reserve space for 3 nodes with an initial room of 100 values each.
    * This will also automaticaly grow if needed. */
    setting.historizingBackend = UA_HistoryDataBackend_Memory(3, 100);
     
    /* We want the server to serve a maximum of 100 values per request.
     * This value depends on the platform you are running the server on - a big
     * server can serve more values in a single request. */
    setting.maxHistoryDataResponseSize = 100;
    // update the database each time the node value is set
    setting.historizingUpdateStrategy = UA_HISTORIZINGUPDATESTRATEGY_VALUESET;
    currentSetting = &setting;

    printf("pre-setting int variable...\n");
    UA_NodeId* nodep = addTestVariable(42);
    //free(*nodep);
    changeInt32Variable2(1, "the.answer", 43);	
    pthread_t thread_id; 
    if (debug)
        printf("Before Thread\n"); 
    struct thread_data td = {millisBetweenChanges, maxChanges, millisBetweenBursts, maxBursts, debug, numTestVals, false};
    pthread_create(&thread_id, NULL, changesThread, (void*) &td); 
    //join forces the current thread to wait until the given thread is finished
    //pthread_join(thread_id, NULL); 
    //printf("After Thread\n"); 
    //sleep(10);

    retval = UA_Server_run(currentServer, &running);
    UA_Server_delete(currentServer);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}


