#include "open62541.h"

#include <stdlib.h>


UA_StatusCode initClient(UA_Client** client_p, char* address, char* user, char* pw)
{
    UA_Client *client = UA_Client_new();
    *client_p = client;
    UA_ClientConfig_setDefault(UA_Client_getConfig(client));
    UA_StatusCode retval = UA_Client_connect_username(client, address, user, pw);
    if(retval != UA_STATUSCODE_GOOD) {
        UA_Client_delete(client);
        return (int)retval;
    }
}

UA_StatusCode readInt(UA_Client* client, UA_NodeId myIntegerNodeId, int* valp)
{
    /* Read the value attribute of the node. UA_Client_readValueAttribute is a
     * wrapper for the raw read service available as UA_Client_Service_read. */
    UA_Variant value; /* Variants can hold scalar values and arrays of any type */
    UA_Variant_init(&value);

    /* NodeId of the variable holding the current time */
    const UA_NodeId nodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_SERVER_SERVERSTATUS_CURRENTTIME);
    UA_StatusCode retval = UA_Client_readValueAttribute(client, nodeId, &value);

    if(retval == UA_STATUSCODE_GOOD && UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_DATETIME])) {
        UA_DateTime raw_date = *(UA_DateTime *) value.data;
        UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "date is: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
    }


    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Preparing for reading Integer node...\n");

    //UA_Variant value; /* Variants can hold scalar values and arrays of any type */
    UA_Variant_init(&value);

    /* NodeId of the variable holding the current time */
    retval = UA_Client_readValueAttribute(client, myIntegerNodeId, &value);

    if(retval == UA_STATUSCODE_GOOD) 
    {
        if (UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_INT32])) 
        {
            UA_Int32 answer = *(UA_Int32 *) value.data;
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "result is: %d\n", answer);
            UA_Variant_clear(&value);
            *valp = answer;
        }
        else
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Reading of node failed: no scalar type found!\n");
        }
    } 
    else
    {
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Reading of node failed: status-code bad.\n");   
    }
    return retval;
}

int main(void) {
    UA_Client* client;
    if(initClient(&client, "opc.tcp://localhost:4840", "paula", "paula123") != UA_STATUSCODE_GOOD) {
        UA_Client_delete(client);
        fprintf(stderr, "Connection failed!");
        return 1;
    }
    
    int val;
    if (readInt(client, UA_NODEID_STRING(1, "the.answer"), &val) != UA_STATUSCODE_GOOD)
    {
        UA_Client_delete(client);
        fprintf(stderr, "Value retrieval failed!");
        return 2;
    }

    /* Clean up */
    UA_Client_delete(client); /* Disconnects the client internally */
    return EXIT_SUCCESS;
}