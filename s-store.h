#include "open62541.h"

#include <signal.h>
#include <stdlib.h>

extern UA_Server* currentServer;
extern UA_ServerConfig* currentConfig;
extern UA_HistoryDataGathering* currentGathering;
extern UA_HistorizingNodeIdSettings* currentSetting;
extern UA_NodeId* storedNodes;
extern bool debug;

extern void instantiateSetting();

extern void registerNodeWithGathering(UA_NodeId* outNodeId);

extern UA_NodeId* 
addVariable(const UA_DataType *type, UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, void *defaultValue, bool historization);

extern UA_NodeId* 
addIntVariable(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, int defaultValue, bool historization);

extern UA_NodeId*
addIntVariable2(UA_NodeId id, UA_QualifiedName name, 
			UA_LocalizedText description, 
			UA_LocalizedText displayName, 
			int defaultValue);

extern UA_NodeId*
addIntVariable3(int id, char* id_str, char* name, int defaultValue, bool historization);

extern UA_NodeId*
addIntVariable4(int id, char* id_str, char* name, int defaultValue);

extern UA_NodeId* 
addStringVariable(UA_NodeId id, UA_QualifiedName name, UA_LocalizedText description, UA_LocalizedText displayName, char* defaultValue, bool historization);

extern UA_NodeId*
addStringVariable2(UA_NodeId id, UA_QualifiedName name, 
			UA_LocalizedText description, 
			UA_LocalizedText displayName, 
			char* defaultValue);

extern UA_NodeId*
addStringVariable3(int id, char* id_str, char* name, char* defaultValue, bool historization);

extern UA_NodeId*
addStringVariable4(int id, char* id_str, char* name, char* defaultValue);

extern UA_NodeId*
addStringVariable5(int id, UA_NodeId nid, char* name, char* defaultValue);

extern UA_NodeId*
addBooleanVariable(int id, char* id_str, char* name, bool defaultValue, bool historization);

extern UA_NodeId*
addStringArrayVariable(int id, char* id_str, char* name, UA_String* defaultValue, int defaultSize, bool historization);

extern UA_NodeId*
addStringArrayVariable2(UA_NodeId nid, char* name, UA_String* defaultValue, int defaultSize, bool historization);

extern UA_NodeId*
addStringArrayVariable4(UA_NodeId nid, char* name, char* defaultValue[], int defaultSize, bool historization);

extern void
changeVariable(UA_NodeId nodeId, const UA_DataType* type, void *value);

extern void
changeArrayVariable(UA_NodeId nodeId, const UA_DataType* type, void* value, int size);

extern void
changeInt32Variable(UA_NodeId myIntegerNodeId, int val);

extern void
changeInt32Variable2(int id, char* strid, int val);

extern void
changeStringVariable(UA_NodeId myIntegerNodeId, char* val);

extern void
changeStringVariable2(int id, char* strid, char* val);

extern void
changeStringArrayVariable(int id, char* id_str, UA_String* value, int size);

extern void
changeStringArrayVariable2(UA_NodeId nid, UA_String* value, int size);

extern void
changeStringArrayVariable3(int id, char* id_str, char* value[], int size);

extern void
changeStringArrayVariable4(UA_NodeId nid, char* value[], int size);

extern void
changeBooleanVariable(int id, char* strid, bool val);

int nsleep(long nsec);
int msleep(long msec);

extern UA_Boolean
allowAddNode(UA_Server *server, UA_AccessControl *ac,
             const UA_NodeId *sessionId, void *sessionContext,
             const UA_AddNodesItem *item);

extern UA_Boolean
allowAddReference(UA_Server *server, UA_AccessControl *ac,
                  const UA_NodeId *sessionId, void *sessionContext,
                  const UA_AddReferencesItem *item);

extern UA_Boolean
allowDeleteNode(UA_Server *server, UA_AccessControl *ac,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_DeleteNodesItem *item);

extern UA_Boolean
allowDeleteReference(UA_Server *server, UA_AccessControl *ac,
                     const UA_NodeId *sessionId, void *sessionContext,
                     const UA_DeleteReferencesItem *item);

UA_StatusCode initiateAccessControl();

void printStoredNode(int i);